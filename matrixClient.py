####################
# Tulsky V.A. 2022 #
####################
from matrix_client.api import MatrixHttpApi
import os
import yaml

# Read YAML file
def read_yaml_file(filename):
    with open(filename, 'r') as file:
        data = yaml.safe_load(file) # returns a dictionary
    return data

# Get the credentials 
def get_creds(dict):
    count = 0
    for entry in dict:
        if entry[tag[0]]==tag[1]:
            matrix_server = entry.get('server') 
            token         = entry.get('token')
            room_id       = entry.get('room_id')
            count += 1
    if count!=1:
        exit( 'Error. Tag \'{}\' value \'{}\' is not unique or doesn\'t exist.'.format(tag[0],tag[1]) )
    else:
        return matrix_server,token,room_id

"""
# ./tokens.yaml file example
---
matrix: 
- server:    "<>" # Matrix - User Settings - Help & About - Advanced - Homeserver
  user:      "<>" # Custom optional tag, can be anything but must be unique
  room_name: "<>" # Custom optional tag, can be anything but must be unique
  room_id:   "<>" # Matrix - Room Settings - Advanced - Room information - Internal room ID
  token:     "<>" # Matrix - User Settings - Help & About - Advanced - Access Token

- server:    "<>"
    ...and so on...
"""

# --- Get the credentials --- #
script_dir = os.path.dirname(__file__) # Get the absolute path to the current script
tokens_file = read_yaml_file(os.path.join(script_dir, "../tokens.yaml"))
tag = ["room_name", "alerts.devops"] # Some unique condition to find the needed credentials
matrix_server, token, room_id = get_creds(tokens_file['matrix']) # at least [matrix_server, token, room_id] should be defined

# --- Define the server --- #
matrix = MatrixHttpApi(matrix_server, token=token)

# --- Prepare a message --- #
message_text = "Hi there! I sent this message using my Python3 script!"

# --- Send a message --- #
response = matrix.send_message(room_id, message_text)
